<?php

/**
 * Admin UI
 */

/*
 * Admin settings page
 */

function jstree_admin_settings_form($form, &$form_state) {

  $form = array();
  $menus = _jstree_menu_menus();

  $form['jstree']['jstree_menu_list'] = array(
    '#type' => 'checkboxes',
    '#title' => t('List'),
    '#options' => $menus,
    '#default_value' => variable_get('jstree_menu_list', array()),
    '#description' => t('jstree will be used for the menus you select here'),
  );

  $form['jstree']['jstree_size_info'] = array(
    '#markup' => t('<strong>To reduce the filesize of the script, you can remove plugins manually by editing the "jquery.jstree.js" file</strong>'),
  );

  return system_settings_form($form);
}



/**
 * Build a human-readable option list for all non-empty menus.
 * Custom menus and book menus are included if the respective modules are enabled.
 * Taken from dhtml_menu
 *
 * @return
 *  an array of avaliable menus key is the menus id, value is the human readable name
 */
function _jstree_menu_menus() {
  if (function_exists('menu_get_menus')) {
    // If the menu module is enabled, list custom menus.
    $menus = menu_get_menus();
  }
  else {
    // Otherwise, list only system menus.
    $menus = menu_list_system_menus();
  }

  // If the book module is enabled, also include book menus.
  if (function_exists('book_get_books')) {
    foreach (book_get_books() as $bid => $link) {
      $menus["book-toc-$bid"] = t('Book: %title', array('%title' => $link['title']));
    }
  }

  // menu_get_names() filters out empty menus, and adds any menus not found using the above calls (edge case).
  foreach (menu_get_names() as $menu) {
    $menu_names[$menu] = isset($menus[$menu]) ? $menus[$menu] : t('Other menu: %menu', array('%menu' => $menu));
  }

  return $menu_names;
}
